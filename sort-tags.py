import gitlab
import os
import pandas as pd

# Replace with your GitLab instance URL
GITLAB_URL = "https://gitlab.com"

# Replace with your GitLab access token
CI_ACCESS_TOKEN=os.environ.get("CI_ACCESS_TOKEN")

# Replace with the project ID or project path
PROJECT_ID = "57060456"

# Create a GitLab instance
gl = gitlab.Gitlab(GITLAB_URL, private_token=CI_ACCESS_TOKEN)

# Get the project
project = gl.projects.get(PROJECT_ID)

# List all tags in the project
tags = project.tags.list(all=True)

# Sort tags by their name (latest first)
sorted_tags = sorted(tags, key=lambda tag: tag.name, reverse=True)

# Get the latest tag
latest_tag = sorted_tags[0] if sorted_tags else None

if latest_tag:
    print(f"The latest tag in the project is: {latest_tag.name}")
    print(f"Tag details:")
    print(f"  Name: {latest_tag.name}")
else:
    print("No tags found in the project.")